<?php

trait Hewan {
	public $nama, $jumlahKaki, $keahlian, $darah = 50;

	public function atraksi() {
		echo "$this->nama Sedang $this->keahlian <br><br>";
	}
}

trait Fight {
	public $attackPower, $defencePower;

	public function serang($pelaku, $korban) {
		echo "$pelaku sedang menyerang $korban <br><br>";
	}

	public function diserang($korban, $attPelaku) {
		$this->darah = $this->darah - $attPelaku / $this->defencePower;

		echo "$korban sedang diserang <br>Sisa darah $korban adalah $this->darah <br><br>";
	}
}

class Elang {
	use Hewan, Fight;

	public $jenis = "Elang";

	public function __construct($nama) {
		$this->jumlahKaki = 2;
		$this->keahlian = "Terbang Tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
		$this->nama = $nama;
	}

	public function getInfoHewan() {
		echo "Nama Hewan: $this->nama <br>Jenis Hewan: $this->jenis <br>Jumlah Kaki: $this->jumlahKaki
			 <br>Keahlian: $this->keahlian <br>Sisa Darah: $this->darah <br>Attack Power: $this->attackPower
			 <br>Defense Power: $this->defencePower <br><br>";
	}
}

class Harimau {
	use Hewan, Fight;

	public $jenis = "Harimau";

	public function __construct($nama) {
		$this->jumlahKaki = 4;
		$this->keahlian = "Lari Cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
		$this->nama = $nama;
	}

	public function getInfoHewan() {
		echo "Nama Hewan: $this->nama <br>Jenis Hewan: $this->jenis <br>Jumlah Kaki: $this->jumlahKaki
			 <br>Keahlian: $this->keahlian <br>Sisa Darah: $this->darah <br>Attack Power: $this->attackPower
			 <br>Defense Power: $this->defencePower <br><br>";
	}
}

$elang = new Elang("Elang_1");
$harimau = new Harimau("Harimau_3");

$elang->atraksi();
$harimau->atraksi();

$elang->serang($elang->nama, $harimau->nama);
$harimau->diserang($harimau->nama, $elang->attackPower);
$harimau->diserang($harimau->nama, $elang->attackPower);

$harimau->serang($harimau->nama, $elang->nama);
$elang->diserang($elang->nama, $harimau->attackPower);
$elang->diserang($elang->nama, $harimau->attackPower);

$elang->getInfoHewan();
$harimau->getInfoHewan();